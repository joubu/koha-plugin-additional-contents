# Koha plugin - Additional contents

This plugin adds the ability to create additional contents and have it integrated in a Koha installation.

## Introduction

DISCLAIMER - This is not ready for production use!

The goal of this plugin is to provide an easy way to create specific contents and have it integrated nicely in a Koha installation.

This plugin will allow you to add links from the Koha mainpage to will point to either external ressources or within your Koha installation.

To test this plugin you will need the patches from [bug 27527](https://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27527) (if not integrated already into the Koha codebase).

## Downloading

From the [release page](https://gitlab.com/Joubu/koha-plugin-additional-contents/-/releases) you can download the relevant *.kpz file

## Installing

The plugin system needs to be turned on by a system administrator.

To set up the Koha plugin system you must first make some changes to your install.

* Change `<enable_plugins>0<enable_plugins>` to `<enable_plugins>1</enable_plugins>` in your koha-conf.xml file
* Confirm that the path to `<pluginsdir>` exists, is correct, and is writable by the web server
* Restart your webserver

On the Tools page you will see the Tools Plugins and on the Reports page you will see the Reports Plugins.

## About Koha plugins

Koha’s Plugin System (available in Koha 3.12+) allows for you to add additional tools and reports to [Koha](http://koha-community.org) that are specific to your library. Plugins are installed by uploading KPZ ( Koha Plugin Zip ) packages. A KPZ file is just a zip file containing the perl files, template files, and any other files necessary to make the plugin work. Learn more about the Koha Plugin System in the [Koha 3.22 Manual](http://manual.koha-community.org/3.22/en/pluginsystem.html) or watch [Kyle’s tutorial video](http://bywatersolutions.com/2013/01/23/koha-plugin-system-coming-soon/).

## License

See the LICENSE file
