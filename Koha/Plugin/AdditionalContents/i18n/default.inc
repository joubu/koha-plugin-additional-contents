[%
    T = {
        plugin_name = "Additional contents",
        configuration = "Configuration",
        add_new_content = "Add new content",
        name = "Name",
        page = "Page",
        content = "Content",
        uri = "URI",
        save_configuration = "Save configuration",
        cancel = "Cancel",
    }
%]
