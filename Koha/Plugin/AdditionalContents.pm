package Koha::Plugin::AdditionalContents;

use Modern::Perl;

use base qw(Koha::Plugins::Base);

our $VERSION = "{VERSION}";

our $metadata = {
    name            => 'Additional contents',
    author          => 'Jonathan Druart',
    description     => 'Add additional contents to your Koha pages',
    date_authored   => '2021-01-20',
    date_updated    => "1970-01-01",
    minimum_version => '20.1200000',
    maximum_version => undef,
    version         => $VERSION,
};

sub new {
    my ( $class, $args ) = @_;

    $args->{'metadata'} = $metadata;
    my $self = $class->SUPER::new($args);

    return $self;
}

sub content {
    my ( $self, $params ) = @_;

    my $table_name = $self->get_qualified_table_name('contents');

    my $id = $params->{id};
    return unless $id;
    my $content = C4::Context->dbh->selectrow_hashref(qq{
        SELECT id, name, content
        FROM $table_name
        WHERE id=?
    }, { Slice => {} }, $id);

    return $content;
}

sub additional_urls {
    my ( $self, $params ) = @_;

    my $page = $params->{page};
    my $table_name = $self->get_qualified_table_name('contents');

    my $contents = C4::Context->dbh->selectall_arrayref(qq{
        SELECT id, name, uri
        FROM $table_name
        WHERE page=?
    }, { Slice => {} }, $page);

    for my $content ( @$contents ) {
        # FIXME This URL must be handled Koha-side
        $content->{uri} ||= sprintf '/cgi-bin/koha/tools/additional_plugin_content.pl?class=%s&id=%s', ref($self), $content->{id};
    }

    return $contents;
}

sub configure {
    my ( $self, $args ) = @_;
    my $cgi = $self->{'cgi'};

    my $table_name = $self->get_qualified_table_name('contents');
    my $op = $cgi->param('op') || 'list';
    my $template = $self->get_template({ file => 'configure.tt' });
    if ( $op eq 'save' ) {
        my $id = $cgi->param('id');
        my $name = $cgi->param('name');
        my $page = $cgi->param('page');
        my $content = $cgi->param('content');
        my $uri = $cgi->param('uri');
        if ( $id ) {
            C4::Context->dbh->do(qq{
                UPDATE $table_name
                SET name=?, page=?, content=?, uri=?
                WHERE id=?
            }, undef, $name, $page, $content, $uri, $id);
        } else {
            C4::Context->dbh->do(qq{
                INSERT INTO $table_name(name, page, content, uri)
                VALUES (?, ?, ?, ?)
            }, undef, $name, $page, $content, $uri);
        }
        $op = 'list';
    }
    elsif ( $op eq 'add_form' ) {
        my $id = $cgi->param('id');
        if ( $id ) {
            my $table_name = $self->get_qualified_table_name('contents');
            my $content = C4::Context->dbh->selectrow_hashref(qq{
                SELECT name, page, content, uri
                FROM $table_name
                WHERE id=?
            }, { Slice => {} }, $id);
            $template->param(
                id => $id,
                name => $content->{name},
                page => $content->{page},
                content => $content->{content},
                uri => $content->{uri},
            );
        }
    }
    elsif ( $op eq 'delete' ) {
        my $id = $cgi->param('id');
        if ( $id ) {
            my $table_name = $self->get_qualified_table_name('contents');
            my $content = C4::Context->dbh->selectrow_hashref(qq{
                DELETE FROM $table_name
                WHERE id=?
            }, { Slice => {} }, $id);
        }
        $op = 'list';
    }

    if ( $op eq 'list' ) {
        my $contents = C4::Context->dbh->selectall_arrayref(qq{
            SELECT id, name, page, content, uri
            FROM $table_name
        }, { Slice => {} });
        $template->param( contents => $contents );
    }
    $template->param(op => $op);
    $self->output_html( $template->output() );
}

sub install {
    my ( $self, $args ) = @_;

    my $table_name = $self->get_qualified_table_name('contents');

    C4::Context->dbh->do(qq{
        CREATE TABLE $table_name (
          `id` INT(11) NOT NULL auto_increment,
          `name` VARCHAR(255) NULL DEFAULT NULL,
          `page` VARCHAR(255) NULL DEFAULT NULL,
          `content` TEXT COLLATE utf8mb4_unicode_ci DEFAULT NULL,
          `uri` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
          PRIMARY KEY  (id)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
    }) unless $self->_table_exists($table_name);

    return 1;
}

sub upgrade {
    my ( $self, $args ) = @_;

    return 1;
}

sub _table_exists {
    my $table = shift;
    eval {
        C4::Context->dbh->{PrintError} = 0;
        C4::Context->dbh->{RaiseError} = 1;
        C4::Context->dbh->do(qq{SELECT * FROM $table WHERE 1 = 0 });
    };
    return 1 unless $@;
    return 0;
}

1;
